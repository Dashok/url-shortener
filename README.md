# URL-shortener #

This is a small project for shortening long URLs. It expects only **http** and **https** schemas by far.

It uses **Java 8**, **Ratpack** libraries, **Guice**, built with **Maven** and backed with **H2** embedded database.

* * *

# API #

### Creation of account: ###

*POST /api/account*

Body: 
```
#!json

{
  "AccountId": "someId"
}
```


If AccountId is not taken, generated password will be returned and Authorization header will be set.

To access account one should provide this header with every subsequent request.

### Shortening an URL: ###

*POST /api/register*

Authorization header must be set.

Body: 
```
#!json

{
  "url": "http://example.com",
  "redirectType": 301
}
```

redirectType is optional, default will be HTTP 302.

Response example: 
```
#!json

{
  "id": "2",
  "origin": "http://example.com",
  "redirectCode": 301,
  "redirects": 0
}
```
where id is shortened path which should be appended to server address like this: **<shortener_host>:<port>/<id>**

### Retrieving statistic of redirects ###

*GET /api/statistic/{accountId}*

Authorization header must be set.


Response example: 


```
#!json

[
  {
    "id": "1",
    "origin": "http://example.com",
    "redirectCode": 301,
    "redirects": 3
  },
  {
    "id": "Oow",
    "origin": "http://example2.com",
    "redirectCode": 302,
    "redirects": 18
  }
]
```


### Getting this README ###

*GET /api/help*

* * *

# Getting started #

### Prerequisites (tested with) ###
* JDK 8
* Maven 3
* Python 2.7 (optional, for tests) with "requests" library

### Build & run ###


```
#!bash
 cd <clone path>/url-shortener
 mvn clean install
 java -jar ./target/urlsh.jar
```

### Running tests ###

**Important note:**

Tests should be executed on empty test database!

DB should be empty because tests use specific username (and if there will be a user creation error tests will fail),

and we need a test DB because tests don't rollback changes.

```
#!bash
 cd <clone path>/url-shortener/test
 python integrationTests.py
```
or for stress test:

```
#!bash
 python stress.py
```

or with Maven:


```
#!bash

mvn test -P integration-tests
mvn test -P stress-test
```