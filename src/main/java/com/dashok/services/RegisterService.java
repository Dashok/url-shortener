package com.dashok.services;

import com.dashok.models.RedirectCode;
import com.dashok.models.RegisteredURL;
import com.dashok.services.impls.RegisterServiceImpl;
import com.google.inject.ImplementedBy;
import org.pac4j.core.profile.UserProfile;
import ratpack.exec.Promise;
import ratpack.service.Service;

import java.net.MalformedURLException;

@ImplementedBy(RegisterServiceImpl.class)
public interface RegisterService extends Service {

  Promise<RegisteredURL> register(String url, RedirectCode code, UserProfile account) throws MalformedURLException;

}
