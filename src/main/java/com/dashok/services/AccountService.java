package com.dashok.services;

import com.dashok.models.Account;
import com.dashok.services.impls.AccountServiceImpl;
import com.google.inject.ImplementedBy;
import ratpack.exec.Promise;

import java.util.Optional;

@ImplementedBy(AccountServiceImpl.class)
public interface AccountService {

  Promise<Account> create(String accountId) throws IllegalArgumentException;

  Optional<Account> getByAccountId(String accountId);

}
