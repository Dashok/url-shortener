package com.dashok.services.impls;

import com.dashok.models.Account;
import com.dashok.repos.DatabaseConnector;
import com.dashok.services.AccountService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.commons.lang3.RandomStringUtils;
import ratpack.exec.Blocking;
import ratpack.exec.Promise;

import java.security.SecureRandom;
import java.util.Optional;

@Singleton
public class AccountServiceImpl implements AccountService {
  private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  private DatabaseConnector databaseConnector;

  @Inject
  public AccountServiceImpl(DatabaseConnector databaseConnector) {
    this.databaseConnector = databaseConnector;
  }

  @Override
  public Promise<Account> create(String accountId) throws IllegalArgumentException {
    return Blocking.get(() -> {
      if (!databaseConnector.getAccountById(accountId).isPresent()) {
        String newPassword = generatePassword();
        databaseConnector.createAccount(accountId, newPassword);
        return new Account(accountId, newPassword);
      }
      else return null;
    });
  }

  @Override
  public Optional<Account> getByAccountId(String accountId) {
    return databaseConnector.getAccountById(accountId);
  }

  private String generatePassword() {
    return RandomStringUtils.random(8, 0, 0, true, true, CHARACTERS.toCharArray(), new SecureRandom());
  }
}
