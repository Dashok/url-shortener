package com.dashok.services.impls;

import com.dashok.models.RedirectCode;
import com.dashok.models.RegisteredURL;
import com.dashok.repos.DatabaseConnector;
import com.dashok.services.RegisterService;
import com.dashok.utils.TimeConverter;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.pac4j.core.profile.UserProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.exec.Blocking;
import ratpack.exec.Promise;
import ratpack.service.DependsOn;
import ratpack.service.StartEvent;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Singleton
@DependsOn(DatabaseConnector.class)
public class RegisterServiceImpl implements RegisterService {
  private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  private DatabaseConnector databaseConnector;
  private volatile String nextShortUrl = "0";
  @Inject
  @Named("settings.update.period")
  private String saveSettingsPeriod;
  private final Object urlGenerationLock = new Object();

  @Inject
  public RegisterServiceImpl(DatabaseConnector databaseConnector) {
    this.databaseConnector = databaseConnector;
  }

  @Override
  public void onStart(StartEvent event) throws Exception {
    init();
  }

  private void init() {
    String nextShortUrlInDb = databaseConnector.getNextShortUrl().orElse(nextShortUrl);

    while (databaseConnector.getRedirectUrl(nextShortUrlInDb, true).isPresent()) {
      nextShortUrlInDb = increment(nextShortUrlInDb);
    }
    nextShortUrl = nextShortUrlInDb;

    ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor(
      new ThreadFactoryBuilder().setNameFormat("lastShortUrlSaver-%d").build());
    scheduler.scheduleAtFixedRate(() -> databaseConnector.saveNextShortUrl(nextShortUrl),
      0, TimeConverter.toMillis(saveSettingsPeriod), TimeUnit.MILLISECONDS);
  }

  @Override
  public Promise<RegisteredURL> register(String url, RedirectCode code, UserProfile account)
    throws MalformedURLException {
    String[] schemes = {"http", "https"};
    UrlValidator urlValidator = new UrlValidator(schemes);
    if (!urlValidator.isValid(url)) {
      throw new MalformedURLException("Url is malformed! If everything seems correct, consider adding http or https schema to the url");
    }
    return Blocking.get(() -> {
      RegisteredURL regUrl = new RegisteredURL();
      regUrl.setOrigin(url);
      regUrl.setRedirectCode(code);
      regUrl.setAccountId(account.getId());

      synchronized (urlGenerationLock) {
        regUrl.setId(nextShortUrl);
        nextShortUrl = increment(nextShortUrl);
      }
      databaseConnector.saveUrl(regUrl);

      return regUrl;
    });
  }

  //0...9->A...Z->a...z->00
  public static String increment(String str) {
    if (str == null) {
      throw new IllegalArgumentException("Value cannot be null!");
    }
    if (StringUtils.containsOnly(str, 'z')) {
      return StringUtils.repeat('0', str.length() + 1); // zzz -> 0000
    }
    StringBuilder s = new StringBuilder(str);
    boolean incrementPrevious = true;
    for (int pos = s.length() - 1; pos >= 0 && incrementPrevious; pos--) {
      char next = getCyclicNextAlphaNumericChar(s.charAt(pos));
      s.setCharAt(pos, next);
      if (next != '0') {
        incrementPrevious = false;
      }
    }
    if (s.toString().equals("api")) { //server api urls start with "api" prefix
      return increment("api");
    }
    return s.toString();
  }

  public static char getCyclicNextAlphaNumericChar(char origin) {
    if (!CharUtils.isAsciiAlphanumeric(origin)) {
      throw new IllegalArgumentException("Value should be only ASCII letter or digit!");
    }
    if (origin == '9') {
      return 'A';
    } else if (origin == 'Z') {
      return 'a';
    } else if (origin == 'z') {
      return '0';
    } else return ++origin;
  }
}
