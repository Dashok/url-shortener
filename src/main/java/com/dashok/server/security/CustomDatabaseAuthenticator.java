package com.dashok.server.security;

import com.dashok.models.Account;
import com.dashok.repos.DatabaseConnector;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.pac4j.core.exception.CredentialsException;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.util.CommonHelper;
import org.pac4j.http.credentials.UsernamePasswordCredentials;
import org.pac4j.http.credentials.authenticator.UsernamePasswordAuthenticator;
import org.pac4j.http.profile.HttpProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Optional;

@Singleton
public class CustomDatabaseAuthenticator implements UsernamePasswordAuthenticator {

  private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  private DatabaseConnector databaseConnector;

  @Inject
  public CustomDatabaseAuthenticator(DatabaseConnector databaseConnector) {
    this.databaseConnector = databaseConnector;
  }

  @Override
  public void validate(UsernamePasswordCredentials credentials) {
    log.info("In CustomDatabaseAuthenticator");
    if (credentials == null) {
      throw new CredentialsException("No credentials!");
    }
    String accountId = credentials.getUsername();
    String password = credentials.getPassword();
    if (CommonHelper.isBlank(accountId)) {
      throw new CredentialsException("accountId cannot be blank");
    }
    if (CommonHelper.isBlank(password)) {
      throw new CredentialsException("Password cannot be blank");
    }

    Optional<Account> account = databaseConnector.getAccountById(accountId);
    if (!account.isPresent() || !password.equals(account.get().getPassword())) {
      throw new CredentialsException("Wrong account id or password!");
    }

    final HttpProfile profile = new HttpProfile();
    profile.setId(accountId);
    profile.addAttribute(CommonProfile.USERNAME, accountId);
//    try {
//      profile.addAttribute("Authorization", "Basic " + new String(Base64.encodeBase64((accountId + ":" + password).getBytes()), "UTF-8"));
//    } catch (UnsupportedEncodingException e) {
//      e.printStackTrace();
//    }
    credentials.setUserProfile(profile);
  }
}
