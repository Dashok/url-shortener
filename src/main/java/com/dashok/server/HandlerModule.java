package com.dashok.server;

import com.dashok.handlers.*;
import com.google.inject.AbstractModule;

public class HandlerModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(AccountHandler.class);
        bind(RegisterHandler.class);
        bind(RedirectHandler.class);
        bind(StatisticsHandler.class);
        bind(HelpHandler.class);
        bind(ErrorHandler.class);
    }
}