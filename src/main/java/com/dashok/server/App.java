package com.dashok.server;

import com.dashok.handlers.*;
import com.dashok.repos.DatabaseConnector;
import com.dashok.server.security.CustomDatabaseAuthenticator;
import com.dashok.services.AccountService;
import com.dashok.services.RegisterService;
import com.dashok.utils.PropertiesLoader;
import com.google.common.collect.ImmutableMap;
import com.google.inject.name.Names;
import org.pac4j.http.client.direct.DirectBasicAuthClient;
import org.pac4j.http.profile.creator.AuthenticatorProfileCreator;
import ratpack.guice.Guice;
import ratpack.hikari.HikariModule;
import ratpack.pac4j.RatpackPac4j;
import ratpack.server.RatpackServer;
import ratpack.session.SessionModule;

import java.util.Properties;

import static com.dashok.utils.PropertiesLoader.Names.*;

public class App {

  public static void main(String... args) throws Exception {

    Properties properties = PropertiesLoader.loadProperties();

    RatpackServer.start(server -> server
//        .serverConfig(ServerConfig.embedded().publicAddress(new URI("http://short.org")))
            .serverConfig(serverConfigBuilder -> serverConfigBuilder
                .props(ImmutableMap.of(PORT.val(), properties.getProperty(PORT.val()),
                        ADDRESS.val(), properties.getProperty(ADDRESS.val()))))
            .registry(Guice.registry(bindings -> bindings
                .module(HikariModule.class, config -> {
                  config.setDataSourceClassName("org.h2.jdbcx.JdbcDataSource");
                  config.addDataSourceProperty("URL",
                      "jdbc:h2:./urlsdb:" + properties.get(DATABASE_NAME.val()) + ";DB_CLOSE_DELAY=-1");
                })
                .binder(binder -> Names.bindProperties(binder, properties))
                .module(SessionModule.class)
                .module(HandlerModule.class)
                .bind(DatabaseConnector.class)
                .bind(AccountService.class)
                .bind(RegisterService.class)
                .bind(CustomDatabaseAuthenticator.class)
            ))
            .handlers(chain -> {
                  chain
                      .register(registry -> registry.add(ErrorHandler.class))
                      .all(RatpackPac4j.authenticator(
                          new DirectBasicAuthClient(chain.getRegistry().get(CustomDatabaseAuthenticator.class),
                              AuthenticatorProfileCreator.INSTANCE)
                      ))
                      .prefix("api", apiChain ->
                          apiChain
                              .get("help", HelpHandler.class)
                              .post("account", AccountHandler.class)
                              .prefix("register", a -> a
                                  .all(RatpackPac4j.requireAuth(DirectBasicAuthClient.class))
                                  .post(RegisterHandler.class)
                              )
                              .prefix("statistic", a -> a
                                  .all(RatpackPac4j.requireAuth(DirectBasicAuthClient.class))
                                  .get(":accountId", StatisticsHandler.class)
                              )
                      )
                      .get("/:id", RedirectHandler.class);
                }
//            .get(":name", ctx -> ctx.render("Hello " + ctx.getPathTokens().get("name") + "!"))
            )
    );
  }
}
