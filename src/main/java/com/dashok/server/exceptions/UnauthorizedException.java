package com.dashok.server.exceptions;

public class UnauthorizedException extends Throwable {

    public UnauthorizedException() {}

    public UnauthorizedException(String message) {
        super(message);
    }
}
