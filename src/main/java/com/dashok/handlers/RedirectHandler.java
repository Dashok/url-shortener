package com.dashok.handlers;

import com.dashok.models.RegisteredURL;
import com.dashok.repos.DatabaseConnector;
import com.dashok.server.exceptions.ResourceNotFoundException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.commons.lang3.StringUtils;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.http.MutableHeaders;

import java.util.Optional;

@Singleton
public class RedirectHandler implements Handler {

    private DatabaseConnector dbConnector;

    @Inject
    public RedirectHandler(DatabaseConnector dbConnector) {
        this.dbConnector = dbConnector;
    }

    @Override
    public void handle(Context context) throws Exception {
        String path = context.getRequest().getPath();
        if(!StringUtils.isAlphanumeric(path)) {
            throw new IllegalArgumentException("Unrecognized request: " + path);
        }

        Optional<RegisteredURL> redirectUrl = dbConnector.getRedirectUrl(path, false);
        RegisteredURL url = redirectUrl.orElseThrow(() ->
            new ResourceNotFoundException("Unrecognized request: " + path));

        MutableHeaders headers = context.getResponse().getHeaders();
        headers.set("Location", url.getOrigin());
        context.getResponse().status(url.getRedirectCode().getCode());
        context.getResponse().send();
    }

}
