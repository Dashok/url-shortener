package com.dashok.handlers;

import com.dashok.models.RegisteredURL;
import com.dashok.repos.DatabaseConnector;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.handling.Context;
import ratpack.handling.Handler;

import java.lang.invoke.MethodHandles;
import java.util.List;

import static ratpack.jackson.Jackson.json;

@Singleton
public class StatisticsHandler implements Handler {
  private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  private DatabaseConnector databaseConnector;

  @Inject
  public StatisticsHandler(DatabaseConnector databaseConnector) {
    this.databaseConnector = databaseConnector;
  }

  @Override
  public void handle(Context context) throws Exception {
    String accountId = context.getPathTokens().get("accountId");
//    String accountId = context.get(UserProfile.class).getId();
    List<RegisteredURL> urls = databaseConnector.getUrlsByAccountId(accountId);
    log.info("urls: {}", urls);
    context.render(json(urls));
  }
}
