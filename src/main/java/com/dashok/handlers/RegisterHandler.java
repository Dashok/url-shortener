package com.dashok.handlers;

import com.dashok.models.RedirectCode;
import com.dashok.services.RegisterService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.pac4j.core.profile.UserProfile;
import ratpack.handling.Context;
import ratpack.handling.Handler;

import static ratpack.jackson.Jackson.fromJson;
import static ratpack.jackson.Jackson.json;

@Singleton
public class RegisterHandler implements Handler {

  private RegisterService registerService;

  @Inject
  public RegisterHandler(RegisterService registerService) {
    this.registerService = registerService;
  }

  public static class RegisterRequest {
    String url;
    RedirectCode redirectType = RedirectCode.DEFAULT;

    public RegisterRequest() {}

    public void setUrl(String url) {
      this.url = url;
    }

    public void setRedirectType(RedirectCode redirectType) {
      this.redirectType = redirectType;
    }

    public void setRedirectType(int type) {
      redirectType = RedirectCode.getByCode(type);
    }

    public String getUrl() {
      return url;
    }

    public RedirectCode getRedirectType() {
      return redirectType;
    }
  }

  @Override
  public void handle(Context context) throws Exception {
    context.parse(fromJson(RegisterRequest.class)).map(n -> {
      if(n == null) {
        throw new IllegalArgumentException("Empty request!");
      }
      return n;
    })
    .then(n ->  registerService.register(n.getUrl(), n.getRedirectType(), context.get(UserProfile.class))
        .then(result -> {
          if(result != null) {
            context.getResponse().status(200);
            context.render(json(result));
          }
          else {
            throw new Exception("Cannot register url " + n.getUrl());
//            context.getResponse().status(500);
//            context.render("error");
          }
        }));
  }

}
