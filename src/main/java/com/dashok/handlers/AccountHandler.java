package com.dashok.handlers;

import static ratpack.jackson.Jackson.json;
import static ratpack.jackson.Jackson.jsonNode;

import com.dashok.services.AccountService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.handling.Context;
import ratpack.handling.Handler;

import java.lang.invoke.MethodHandles;

@Singleton
public class AccountHandler implements Handler {

  private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
  private AccountService accountService;

  @Inject
  public AccountHandler(AccountService accountService) {
    this.accountService = accountService;
  }

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  public class AccountResponse {
    private Boolean success;
    private String description;
    private String password;

    public AccountResponse(Boolean success, String description, String password) {
      this.success = success;
      this.description = description;
      this.password = password;
    }

    public AccountResponse(Boolean success, String description) {
      this.success = success;
      this.description = description;
    }

    public Boolean getSuccess() {
      return success;
    }

    public String getDescription() {
      return description;
    }

    public String getPassword() {
      return password;
    }
  }

  @Override
  public void handle(Context context) throws Exception {
    context.parse(jsonNode()).map(n -> {
      JsonNode accountId = n.get("AccountId");
      if(accountId == null) {
        throw new IllegalArgumentException("AccountId must be specified!");
      }
      return accountId.asText();
    })
    .then(accountId ->  accountService.create(accountId)
        .then(result -> {
          if(result != null) {
            AccountResponse successfullResponse = new AccountResponse(true,
                    "Account was successfully created!", result.getPassword());
            context.getResponse().status(200);
            context.getResponse().getHeaders().set("Authorization", "Basic " +
                    new String(Base64.encodeBase64((accountId + ":" + result.getPassword()).getBytes()), "UTF-8"));
            context.render(json(successfullResponse));
          }
          else {
            AccountResponse faultyResponse = new AccountResponse(false,
                    "Account with such id already exists!");
            context.getResponse().status(409);
            context.render(json(faultyResponse));
          }
        }));
  }
}
