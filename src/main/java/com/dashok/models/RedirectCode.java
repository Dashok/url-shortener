package com.dashok.models;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;

public enum RedirectCode {
  DEFAULT (302),
  MOVED_PERMANENTLY(301),
  MOVED_TEMPORARILY(302);

  private int code;
  private static HashMap<Integer, RedirectCode> lookup = new HashMap<>();

  static {
    for (RedirectCode c :
            RedirectCode.values()) {
      lookup.put(c.getCode(), c);
    }
  }

  RedirectCode(int code) {
    this.code = code;
  }

  @JsonValue
  public int getCode() {
    return code;
  }

  public static RedirectCode getByCode(int code) {
    RedirectCode rCode = lookup.get(code);
    return rCode != null ? rCode : DEFAULT;
  }
}
