package com.dashok.models;

import java.util.List;

public class Account {

  private String accountId;
  private String password;
  private List<RegisteredURL> urls;

  public Account(String accountId, String password) {
    this.accountId = accountId;
    this.password = password;
  }

  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<RegisteredURL> getUrls() { //todo: deep copy
    return urls;
  }

  public void setUrls(List<RegisteredURL> urls) { //todo: deep copy
    this.urls = urls;
  }
}
