package com.dashok.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.concurrent.atomic.AtomicLong;

public class RegisteredURL {

  private String id;
  private String origin;
  private RedirectCode redirectCode = RedirectCode.DEFAULT;
  private AtomicLong redirects = new AtomicLong(0);

  @JsonIgnore
  private String accountId;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public RedirectCode getRedirectCode() {
    return redirectCode;
  }

  public void setRedirectCode(RedirectCode redirectCode) {
    this.redirectCode = redirectCode;
  }

  public long getRedirects() {
    return redirects.get();
  }

  public void setRedirects(long redirects) {
    this.redirects.set(redirects);
  }

  public long incrementRedirects() {
    return redirects.incrementAndGet();
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public String getAccountId() {
    return accountId;
  }

}
