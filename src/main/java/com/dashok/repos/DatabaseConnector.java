package com.dashok.repos;

import com.dashok.models.Account;
import com.dashok.models.RedirectCode;
import com.dashok.models.RegisteredURL;
import com.dashok.utils.LruCache;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.service.Service;
import ratpack.service.StartEvent;

import javax.sql.DataSource;
import java.lang.invoke.MethodHandles;
import java.sql.*;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Singleton
public class DatabaseConnector implements Service {

  private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  @Inject
  @Named("cache.maxsize")
  private int maxCacheSize;
  private DataSource dataSource;
  private Map<String, RegisteredURL> urlsCache; //todo heat cache (select max redirects with limit n?)

  public DatabaseConnector() {
    urlsCache = new LruCache<>(maxCacheSize);
  }

  @Override
  public void onStart(StartEvent event) throws Exception {
    dataSource = event.getRegistry().get(DataSource.class);
    if (!dbInitialized()) {
      initializeDb();
    }
  }

  private boolean dbInitialized() throws SQLException {
    log.debug("Check database");
    List<String> requiredTables = Arrays.asList("accounts", "urls", "accounts_to_urls", "settings");
    List<String> existingTables = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      Statement st = connection.createStatement();
      ResultSet result = st.executeQuery("SHOW TABLES");
      while (result.next()) {
        existingTables.add(result.getString("TABLE_NAME"));
      }
    }

    boolean initialized = existingTables.stream().map(String::toLowerCase)
        .filter(requiredTables::contains).count() == requiredTables.size();

    if (initialized) {
      log.debug("Database already initialized");
    }
    return initialized;
  }

  private void initializeDb() throws SQLException {
    log.debug("Initialize database");
    try (Connection connection = dataSource.getConnection()) {
      connection.createStatement().executeUpdate("CREATE TABLE accounts (account_id VARCHAR(255) PRIMARY KEY, password VARCHAR(10) NOT NULL);");
      connection.createStatement().executeUpdate("CREATE TABLE urls (id VARCHAR(1024) NOT NULL PRIMARY KEY, origin VARCHAR(1024) NOT NULL, redirection_code INT DEFAULT 302, num_redirects BIGINT DEFAULT 0);");
      connection.createStatement().executeUpdate("CREATE TABLE accounts_to_urls (account_id VARCHAR(255) NOT NULL, url_id VARCHAR(1024) NOT NULL);");
      connection.createStatement().executeUpdate("CREATE TABLE settings (id BOOLEAN DEFAULT TRUE PRIMARY KEY, next_short_url VARCHAR(1024) NOT NULL,  CHECK(id));");
      connection.createStatement().executeUpdate("ALTER TABLE accounts_to_urls ADD FOREIGN KEY (account_id) REFERENCES accounts(account_id);");
      connection.createStatement().executeUpdate("ALTER TABLE accounts_to_urls ADD FOREIGN KEY (url_id) REFERENCES urls(id);");
    }
  }

  // Account API

  public boolean createAccount(String accountId, String password) {
    String insertQuery = "INSERT INTO accounts (account_id, password) VALUES(?, ?);";
    try (Connection conn = dataSource.getConnection()) {
      PreparedStatement pst = conn.prepareStatement(insertQuery);
      pst.setString(1, accountId);
      pst.setString(2, password);
      pst.executeUpdate();
      return true;
    }
    catch (SQLException ex) {
      log.error("Can't create new account, failed: {}", ex);
    }
    return false;
  }

  public Optional<Account> getAccountById(String accountId) {
    String sql = "SELECT * FROM accounts WHERE account_id = ?;";
    try (Connection conn = dataSource.getConnection()) {
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, accountId);
      ResultSet resultSet = pst.executeQuery();
      if (resultSet.next()) {
        return Optional.of(new Account(accountId, resultSet.getString("password")));
      }
    }
    catch (SQLException ex) {
      log.error("Error while retrieving account by id: {}!", accountId, ex);
    }
    return Optional.empty();
  }

  // Statistics API

  public List<RegisteredURL> getUrlsByAccountId(String accountId) {
    String sql = "SELECT * FROM urls INNER JOIN accounts_to_urls ON urls.id = accounts_to_urls.url_id WHERE account_id = ?;";
    List<RegisteredURL> urls = new ArrayList<>();
    try (Connection conn = dataSource.getConnection()) {
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, accountId);

      ResultSet resultSet = pst.executeQuery();
      while (resultSet.next()) {
        RegisteredURL url = new RegisteredURL();
        url.setId(resultSet.getString("id"));
        url.setOrigin(resultSet.getString("origin"));
        url.setRedirectCode(RedirectCode.getByCode(resultSet.getInt("redirection_code")));
        url.setRedirects(resultSet.getLong("num_redirects"));
        urls.add(url);
      }
    }
    catch (SQLException ex) {
      log.error("Error while retrieving urls for accountId: {}!", accountId, ex);
    }
    return urls;
  }

  // Register URL API

  public RegisteredURL saveUrl(RegisteredURL regUrl) throws Exception {
    if (regUrl == null) {
      return null;
    }

    String urlInsertSql = "INSERT INTO urls (id, origin, redirection_code) VALUES (?, ?, ?)";
    String accs2urlsSql = "INSERT INTO accounts_to_urls (account_id, url_id) VALUES (?, ?)";

    Connection conn = dataSource.getConnection();
    try {
      // prepare transaction
      conn.setAutoCommit(false);
      int code = (regUrl.getRedirectCode() != null)
          ? regUrl.getRedirectCode().getCode()
          : RedirectCode.DEFAULT.getCode();


      PreparedStatement urlInsertStm = conn.prepareStatement(urlInsertSql);
      urlInsertStm.setString(1, regUrl.getId());
      urlInsertStm.setString(2, regUrl.getOrigin());
      urlInsertStm.setInt(3, code);

      PreparedStatement accUrlInsertStm = conn.prepareStatement(accs2urlsSql);
      accUrlInsertStm.setString(1, regUrl.getAccountId());
      accUrlInsertStm.setString(2, regUrl.getId());

      // run transaction
      urlInsertStm.executeUpdate();
      accUrlInsertStm.executeUpdate();

      // everything is fine
      conn.commit();

      // put new url into the cache
      urlsCache.put(regUrl.getId(), regUrl);
    }
    catch (SQLException ex) {
      // transaction failed
      conn.rollback();
      String error = "Error while saving url with id: " + regUrl.getId();
      log.error(error, ex);
      throw new RuntimeException(error);
    } finally {
      conn.close();
    }
    return regUrl;
  }

  // Redirect API

  /**
   * @param id short url
   * @param silently if it is true, redirect statistics will not be updated
   */
  public Optional<RegisteredURL> getRedirectUrl(String id, boolean silently) {
    RegisteredURL url;

    // try to find url anywhere
    if (!urlsCache.containsKey(id)) {
      Optional<RegisteredURL> optUrl = getRegisteredUrlById(id);
      if (!optUrl.isPresent()) {
        return Optional.empty(); // url not found
      }
      url = optUrl.get();
      urlsCache.put(id, url);
    } else {
      url = urlsCache.get(id);
    }

    if (!silently) {
      // run statistic updater
      CompletableFuture.runAsync(() -> {
        incrementUrlRedirects(id);
        log.debug("Redirects number for url {} was updated", id);
      });
    }

    // return founded url
    return Optional.of(url);
  }

  private Optional<RegisteredURL> getRegisteredUrlById(String id) {
    String sql = "SELECT * FROM urls WHERE id = ?;";
    try(Connection conn = dataSource.getConnection()) {
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, id);
      ResultSet resultSet = pst.executeQuery();
      if (resultSet.next()) {
        RegisteredURL url = new RegisteredURL();
        url.setId(resultSet.getString("id"));
        url.setOrigin(resultSet.getString("origin"));
        url.setRedirectCode(RedirectCode.getByCode(resultSet.getInt("redirection_code")));
        url.setRedirects(resultSet.getLong("num_redirects"));
        return Optional.of(url);
      }
    } catch (SQLException ex) {
      log.error("Error while retrieving url by id: id = {}, ex = {}", id, ex);
    }
    return Optional.empty();
  }

  private void incrementUrlRedirects(String id) {
    String sqlUpd = "UPDATE urls SET num_redirects = num_redirects + 1 WHERE id = ?;";
    try(Connection conn = dataSource.getConnection()) {
      PreparedStatement pst = conn.prepareStatement(sqlUpd);
      pst.setString(1, id);
      pst.executeUpdate();
    } catch (SQLException ex) {
      log.error("Error while incrementing url redirects number: id = {}, ex = {}", id, ex);
    }
  }

  public Optional<String> getNextShortUrl() {
    String sql = "SELECT next_short_url FROM settings;";
    try(Connection conn = dataSource.getConnection()) {
      PreparedStatement pst = conn.prepareStatement(sql);
      ResultSet resultSet = pst.executeQuery();

      if (resultSet.next()) {
        return Optional.of(resultSet.getString("next_short_url"));
      }

    } catch (SQLException ex) {
      throw new RuntimeException("Exception while retrieving next short url from database", ex);
    }
    return Optional.empty();
  }

  public void saveNextShortUrl(String nextShortUrl) {
    String sqlUpd = "UPDATE settings SET next_short_url = ?;";
    try(Connection conn = dataSource.getConnection()) {
      PreparedStatement pst = conn.prepareStatement(sqlUpd);
      pst.setString(1, nextShortUrl);
      pst.executeUpdate();
    } catch (SQLException ex) {
      log.error("Error while saving last short url = {}, ex = {}", nextShortUrl, ex);
    }
  }
}
