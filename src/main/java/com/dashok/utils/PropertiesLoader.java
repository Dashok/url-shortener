package com.dashok.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.Properties;

import static com.dashok.utils.PropertiesLoader.Names.*;

public class PropertiesLoader {
  private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
  private static final String PROPERTIES_FILE = "app.properties";
  private static Properties defaults = new Properties();

  static {
    defaults.setProperty(PORT.val(), "5070");
    defaults.setProperty(ADDRESS.val(), "localhost");
    defaults.setProperty(MAX_CACHE_SIZE.val(), "10000");
    defaults.setProperty(DATABASE_NAME.val(), "urlshortener");
    defaults.setProperty(SAVE_SETTINGS_PERIOD.val(), "5s");
  }

  public enum Names {
    PORT("server.port"),
    ADDRESS("server.address"),
    MAX_CACHE_SIZE("cache.maxsize"),
    DATABASE_NAME("db.name"),
    SAVE_SETTINGS_PERIOD("settings.update.period");

    private String value;

    Names(String value) {
      this.value = value;
    }

    public String val() {
      return value;
    }
  }

  public static Properties loadProperties() {
    return loadProperties(PROPERTIES_FILE);
  }

  public static Properties loadProperties(String file) {
    Properties properties = new Properties(defaults);
    try {
      InputStream stream = PropertiesLoader.class.getClassLoader().getResourceAsStream(file);
      if (stream != null) {
        properties.load(stream);
      } else {
        log.warn("Properties file {} was not loaded. Defaults were applied.", file);
      }
    } catch (IOException e) {
      log.warn("Could not load properties from file {}! Defaults were applied.", file, e);
    }
    return properties;
  }
}
