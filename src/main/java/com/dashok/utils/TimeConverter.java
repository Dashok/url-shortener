package com.dashok.utils;

import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.*;

public class TimeConverter {

  public static long toMillis(String numericWithTimeUnit) {
    final TimeUnit timeUnit = parseTimeUnit(numericWithTimeUnit);
    return timeUnit.toMillis(Long.parseLong(numericWithTimeUnit.replaceAll("[^0-9]", "")));
  }

  private static TimeUnit parseTimeUnit(String str) {
    final String timeUnit = str.replaceAll("[0-9 ]", "");
    switch (timeUnit) {
      case "d": return DAYS;
      case "h": return HOURS;
      case "m": return MINUTES;
      case "s": return SECONDS;
      default: return MILLISECONDS;
    }
  }

}
