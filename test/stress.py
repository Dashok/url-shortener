#!/usr/bin/python

import json
import pprint
import random
import requests
import sys
import time
from threading import Thread

URL = 'http://localhost:5060'
API_URL = URL + '/api'
ACC_URL = API_URL + '/account'
REGISTER_URL = API_URL + '/register'
USER = 'stress_test'

REQUEST_CNT = 500
WAVES = 10

# acc creation
print "\nTesting account creation"
response = requests.post(ACC_URL, json = {"AccountId":USER})
print "User creation response:"
pprint.pprint(response.json())
auth_header = response.headers["Authorization"]

# routine to store urls
def store_urls(urls, thread_name):
    start_time = time.time()
    for i in range(1, WAVES):
        print thread_name, ": ", (i * REQUEST_CNT), '\t%.2f' % (time.time() - start_time), "s"
        sys.stdout.flush()
        for j in range(1, REQUEST_CNT):
            requests.post(REGISTER_URL, json = {"url": random.choice(urls)}, headers={"Authorization":auth_header})
    print "RESULT: ", thread_name, ": ", (WAVES*REQUEST_CNT), '\t%.2f' % (time.time() - start_time), "s"

# routine to get urls
def get_urls(urls, thread_name):
    start_time = time.time()
    for i in range(1, WAVES):
        print thread_name, ": ", (i * REQUEST_CNT), '\t', (time.time() - start_time)
        sys.stdout.flush()
        for j in range(1, REQUEST_CNT):
            requests.get(random.choice(urls), allow_redirects=False)
    print thread_name, ": ", (WAVES*REQUEST_CNT), '\t', (time.time() - start_time)

# test script
with open("urls.json", 'r') as urls_file:
    urls = json.load(urls_file)

t1 = Thread( target=store_urls, args=(urls.values(), "Producer-1") )
t2 = Thread( target=store_urls, args=(urls.values(), "Producer-2") )
t3 = Thread( target=store_urls, args=(urls.values(), "Producer-3") )
t4 = Thread( target=store_urls, args=(urls.values(), "Producer-4") )
t5 = Thread( target=store_urls, args=(urls.values(), "Producer-5") )

print "t1 start"
t1.start()
print "t2 start"
t2.start()
print "t3 start"
t3.start()
#t4.start()
#t5.start()

t1.join()
print "t1 stop"
t2.join()
print "t2 stop"
t3.join()
print "t3 stop"
#t4.join()
#t5.join()