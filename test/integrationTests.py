#!/usr/bin/python

import json
import pprint
import random
import requests

# todo test unsuccessful cases

URL = 'http://localhost:5060'
API_URL = URL + '/api'
ACC_URL = API_URL + '/account'
REGISTER_URL = API_URL + '/register'
STATISTIC_URL = API_URL + '/statistic'
USER = 'test'

# acc creation
print "\nTesting account creation"
response = requests.post(ACC_URL, json = {"AccountId":USER})
print "User creation response:"
pprint.pprint(response.json())

auth_header = response.headers["Authorization"]

# url registration
print "\nTesting url registration"
with open("urls.json", 'r') as urls_file:
    urlsmap = json.load(urls_file)
registered = []
for key, url in urlsmap.iteritems():
    response = requests.post(REGISTER_URL, json = {"url":url}, headers={"Authorization":auth_header})
    jsonResp = response.json()
    print url, " registration response:"
    pprint.pprint(response.json())
    registered.append(jsonResp["id"])

# requesting registered urls
print "\nTesting short urls requesting"
reqested_urls = {}
for i in range(0, 10):
    current_url = random.choice(registered)
    response = requests.get(URL + "/" + current_url)
    if not current_url in reqested_urls:
        reqested_urls[current_url] = 1
    else:
        reqested_urls[current_url] += 1
    print "Response for short_url", current_url, response.url
print "Urls to requests count: ", reqested_urls

# statistic
print "\nTesting statistic"
response = requests.get(STATISTIC_URL + '/' + USER, headers={"Authorization":auth_header})
print "Statistic response:"
pprint.pprint(response.json())
